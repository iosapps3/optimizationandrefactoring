# OptimizationAndRefactoring

## About the App
- This app shows the information of the schools from New York city. It contains two screens where first screen displays all the schools from New York City. 
- You can utilize the search option and get filtered results of all the matched school names
- When any school is selected, it takes you to school's detail screen where contact information, overview, SAT scores are provided for that selected schools.
- Location of the school can be seen by clicking on map button which is available in detail screen at top right corner

## App Flow
<img src="Resources/AppFlow.png" width="400">

## App Screens
<img src="Resources/SchoolsListScreen.png" width="250">

<img src="Resources/SchoolDetails.png" width="250">

<img src="Resources/Map.png" width="250">

## Authors and acknowledgment
Dedeepya Salla
