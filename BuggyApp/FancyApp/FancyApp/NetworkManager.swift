import Foundation
import UIKit

protocol NetworkManagerDelegate {
	func didFinishFetching(data: Data?, error: Error?)
}

class NetworkManager {
    var delegate: NetworkManagerDelegate
    
	init(delegate: NetworkManagerDelegate) {
		self.delegate = delegate
	}
	
	func fetchData() {
        guard let url = URL(string: "https://opencollective.com/webpack/members.json?limit=10&offset=0") else {
            return
        }
        
		URLSession.shared.dataTask(with: url) { (data, response, error) in
			self.delegate.didFinishFetching(data: data, error: error)
        }.resume()
    }
}
