import Foundation
import UIKit

class ListViewController: UIViewController, UITableViewDataSource, NetworkManagerDelegate {
	let tableView = UITableView()
	var jsonList: [[String: Any]]?
	
	var loading = false
	var displaying = true
	lazy var networkManager = NetworkManager(delegate: self)
	
	override func viewDidLoad() {
		super.viewDidLoad()
		view.backgroundColor = .systemMint
		
		networkManager.fetchData()
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		jsonList?.count ?? 0
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
		cell.textLabel?.text = jsonList?[indexPath.row]["name"] as? String
		return cell
	}
	
	func didFinishFetching(data: Data?, error: Error?) {
		if let error = error {
			self.alert()
		}
		
		if let data = data {
			do {
				if let jsonList = try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]] {
					self.jsonList = jsonList
				}
			} catch let error as NSError {
				print("Failed to load: \(error.localizedDescription)")
			}
		}
		
		self.makeList()
		self.alertTwo()
	}
	
	func makeList() {
		view.addSubview(tableView)
		tableView.backgroundColor = .systemMint
		tableView.translatesAutoresizingMaskIntoConstraints = false
		tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
		tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
		tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
		tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
		tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
		tableView.dataSource = self
	}
	
	func alert() {
		let alertController = UIAlertController(title: "Error!",
												message: "Mic drop!",
												preferredStyle: .alert)
		let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in }
		alertController.addAction(cancelAction)
		present(alertController, animated: true, completion: nil)
	}
	
	func alertTwo() {
		let alertController = UIAlertController(title: "List loaded!",
												message: "Mic drop!",
												preferredStyle: .alert)
		let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in }
		alertController.addAction(cancelAction)
		present(alertController, animated: true, completion: nil)
	}
}
