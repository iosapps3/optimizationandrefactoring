import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        view.backgroundColor = .orange
        
        let button = UIBarButtonItem(title: "Add",
                                     style: .plain,
                                     target: self,
                                     action: #selector(showList))
        navigationItem.rightBarButtonItem = button
    }
    
    @objc
    func showList() {
//        let viewController = ListViewController()
//        let networkManager = NetworkManager()
//        networkManager.delegate = viewController
//        viewController.networkManager = networkManager
        
        let networkManager = NetworkManager()
        let viewController = ListViewController()
        navigationController?.pushViewController(viewController, animated: true)
    }
}
