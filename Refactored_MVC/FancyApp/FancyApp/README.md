#  eBay Payments iOS Screen: Debugging Round

## Introduction
The provided ``FancyApp`` project contains two views. 
The first, ``ViewController`` is a simple blank view containing a button to navigate to the ``ListViewController``. 
The ``ListViewController`` populates a ``UITableView`` using a JSON response fetched from a network call via the ``NetworkManager`` class. 

## Debugging Objectives

With the provided ``FancyApp`` project, get it running and use your time to improve it in the following areas, as much as the time allows:

- [ ] The ``NetworkManager`` class holds onto ``ListViewController`` as a delegate, and vice versa. Identify any potential areas for improvement here to avoid future issues.
- [ ] The ``NetworkManager`` uses the Delegate pattern in order to update the ``ListViewController`` it is finished fetching. Identify an approach better suited to this use case than the Delegate pattern.
- [ ] Use the ``Result`` type to return data response / errors from our network call back to the view controller.
- [ ] Our `ListViewController` is doing too much in one class. Explore potential rearchitecture of the view controller to improve this project.
- [ ] Add unit tests for this project. Ensure asynchronous functionality is tested adequately.
- [ ] BONUS: If time allows, update UI to show image received in JSON response.
