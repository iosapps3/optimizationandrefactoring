import Foundation
import UIKit

protocol NetworkManagerDelegate: AnyObject {
	func didFinishFetching(data: Data?, error: Error?)
}

protocol DataFetcher {
    func fetchData()
    var delegate: NetworkManagerDelegate? { get set }
    func fetchData1(completion: @escaping ((Result<Data, Error>) -> Void) )
}

class NetworkManager: DataFetcher {
    weak var delegate: NetworkManagerDelegate?
    
//	init(delegate: NetworkManagerDelegate) {
//		self.delegate = delegate
//	}
	
    
	func fetchData() {
        guard let url = URL(string: "https://opencollective.com/webpack/members.json?limit=10&offset=0") else {
            return
        }
        
		URLSession.shared.dataTask(with: url) { (data, response, error) in
			self.delegate?.didFinishFetching(data: data, error: error)
        }.resume()
    }
    
    func fetchData1(completion: @escaping ((Result<Data, Error>) -> Void) ) {
        guard let url = URL(string: "https://opencollective.com/webpack/members.json?limit=10&offset=0") else {
            return
        }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            if let data = data {
                completion(.success(data))
            }
           
//            completion(.failure(<#T##Error#>))
//            self.delegate?.didFinishFetching(data: data, error: error)
        }.resume()
    }
}
