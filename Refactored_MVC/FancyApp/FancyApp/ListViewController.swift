import UIKit


class ListViewController: UIViewController {
    
    var executeOnMain: (@escaping () -> Void) -> Void = { completion in
        DispatchQueue.main.async {
            completion()
        }
    }
    
	let tableView = UITableView()
	var jsonList: [[String: Any]]?
	var loading = false
	var displaying = true
    /*
     err:
     let networkManager: DataFetcher
     Missing argument for parameter 'networkManager' in call
     
     err:
     var networkManager: DataFetcher! --> not safe
     
     unit tests:
     initial load --> check networkmanager
     check viewDidload --> tableview 0 rows --> all below should not work if 0 rows
     once data is received --> check tabelview rows count, also check if tableview is added to view or not, tableview background or something, tableviewdatasournce
     
     if error -> based on error --> two kinds of alerts
     */
   
    var networkManager: DataFetcher?
    convenience init(networkManager: DataFetcher) {
        self.init()
        self.networkManager = networkManager
    }
    
    let test = NetworkManager()
	override func viewDidLoad() {
		super.viewDidLoad()
		view.backgroundColor = .systemMint
		//
       
        test.delegate = self
        test.fetchData()
        networkManager?.fetchData1 { result in
            switch result {
            case .success(let data):
                self.executeOnMain {
                    self.makeList()
                }
            case .failure(let error):
                print("Failed to load: \(error.localizedDescription)")
                self.executeOnMain {
                    self.showAlert(title: "List loaded!", message: "Mic drop!", actionTitle: "Cancel")
                }
            }
        }
        
        updateNameSync()
	}
    
  //  private var name: String = "mainName"
//    func updateName () {
//        print("-----------------------")
//        DispatchQueue.global().async {
//            self.recordAndCheckWrite(self.name)
//            self.name.append( "Antoine van der Lee")
//            print("DispatchQueue.global () .async") //, self.name)
//            //print(self.name)
//        }
//
//        print("main queue")
//        self.recordAndCheckWrite(self.name) // Added by the compiler
//        print(self.name)
//        print("-----------------------")
//    }
//
//    func recordAndCheckWrite(_ str: String) {
//        print("recordAndCheckWrite-----------------------", str)
//        print(Date())
//        print(Thread.current)
//        print("recordAndCheckWrite-----------------------", str)
//    }

    private let lockQueue = DispatchQueue(label: "name.lock.queue")
    private var name: String = "first ---"

    func updateNameSync() {
        DispatchQueue.global().sync {
            self.lockQueue.sync {
                self.name.append("Antoine van der Lee")
                print(self.name)
            }
        }

        // Executed on the Main Thread
        lockQueue.sync {
            // Executed on the lock queue
            print(self.name)
        }
    }
	
	func makeList() {
		view.addSubview(tableView)
		tableView.backgroundColor = .systemMint
		tableView.translatesAutoresizingMaskIntoConstraints = false
		tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
		tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
		tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
		tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
		tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
		tableView.dataSource = self
	}
	
    func showAlert(title: String, message: String, actionTitle: String) {
        let alertController = UIAlertController(title: title,
                                                message: message,
                                                preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: actionTitle, style: .cancel) { _ in }
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
}

extension ListViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        jsonList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = jsonList?[indexPath.row]["name"] as? String
        return cell
    }
    
}

extension ListViewController : NetworkManagerDelegate {
    
    func didFinishFetching(data: Data?, error: Error?) {
        if error != nil {
            self.showAlert(title: "Error!", message: "Mic drop!", actionTitle: "Cancel")
            return
        }
        
        if let data = data {
            do {
                if let jsonList = try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]] {
                    self.jsonList = jsonList
                    self.executeOnMain {
                        self.makeList()
                    }
                }
            } catch let error as NSError {
                print("Failed to load: \(error.localizedDescription)")
                self.executeOnMain {
                    self.showAlert(title: "List loaded!", message: "Mic drop!", actionTitle: "Cancel")
                }
               
            }
        }
    }
}
